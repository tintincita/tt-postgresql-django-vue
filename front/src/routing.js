import { createRouter, createWebHistory } from "vue-router"


import AlarmsList from './components/AlarmsList.vue'
import ProducerAlarmsList from './components/ProducerAlarmsList.vue'
import AlarmsReport from './components/AlarmsReport.vue'


const router = createRouter({
    history: createWebHistory(),
    routes: [
        // Base route
        {path: '/', redirect: '/alarms'},

        // Alarms routes
        {path: '/alarms', component: AlarmsList},
        {path: '/producer_alarms', component: ProducerAlarmsList},
        {path: '/alarms_report', component: AlarmsReport}
 
        // {path: '/people/details/:id', component: PeopleDetails},
        // {path: '/people/delete/:id', component: PeopleDelete},

    ]
})

export default router