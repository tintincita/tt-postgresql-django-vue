import { createApp } from 'vue'
import router from './routing.js'

import App from './App.vue'

createApp(App).use(router).mount('#app')