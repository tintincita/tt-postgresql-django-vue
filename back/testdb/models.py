from django.db import models

# Create your models here.
class Alarm(models.Model):
    time = models.DateField()
    alarm_code = models.IntegerField()
    producer_id = models.IntegerField()
    created_by_id= models.IntegerField()
    created_at = models.DateField()
    updated_at= models.DateField()
    updated_by_id = models.IntegerField()