from rest_framework import serializers
from .models import Alarm, ReportLine


class AlarmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alarm
        fields = '__all__'


class ProducerAlarmsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Alarm
        fields = '__all__'


class ReportLineSerializer(serializers.Serializer):
    """Your data serializer, define your fields here."""
    producer_id = serializers.IntegerField()
    alarm_code = serializers.IntegerField()
    count = serializers.IntegerField()
    level = serializers.CharField()
    name = serializers.CharField()
    code = serializers.CharField()
    criticality = serializers.CharField()