from django.contrib import admin

# Register your models here.

from .models import Alarm


class AlarmAdmin(admin.ModelAdmin):
    list_display = ('time', 'alarm_code', 'producer_id',
                    'created_by_id', 'created_at', 'updated_at', 'updated_by_id')




admin.site.register(Alarm, AlarmAdmin)
