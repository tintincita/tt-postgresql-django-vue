from django.db import models
import logging

logger = logging.getLogger(__name__)


class AlarmCode(models.Model):
    level = models.CharField(max_length=50, null=True)
    name = models.CharField(max_length=500)
    code = models.CharField(max_length=6)
    criticality = models.CharField(max_length=50, null=True)
    enabled = models.BooleanField()
    producer_type_id = models.IntegerField()
    created_at = models.DateTimeField()
    created_by_id = models.IntegerField()
    updated_at = models.DateTimeField()
    updated_by_id = models.IntegerField()


class Alarm(models.Model):
    time = models.DateTimeField()
    alarm_code = models.ForeignKey(AlarmCode, on_delete=models.CASCADE)
    # alarm_code_id = models.IntegerField()
    producer_id = models.IntegerField()
    created_by_id = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    updated_by_id = models.IntegerField()


class ReportLine(models.Model):
    producer_id = models.IntegerField()
    count = models.IntegerField()
    alarm_code = models.ForeignKey(AlarmCode, on_delete=models.CASCADE)

class Line:
    pass