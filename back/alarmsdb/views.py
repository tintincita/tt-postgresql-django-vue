from django.shortcuts import render
from django.db.models import Count
from rest_framework import generics, viewsets
import logging

from .serializers import AlarmSerializer, ProducerAlarmsSerializer, ReportLineSerializer
from .models import Alarm, AlarmCode, Line, ReportLine


# Create your views here.
logger = logging.getLogger(__name__)


class AlarmView(viewsets.ModelViewSet):
    serializer_class = ProducerAlarmsSerializer
    queryset = Alarm.objects.all()


class ProducerAlarmsView(viewsets.ViewSetMixin, generics.ListAPIView):
    serializer_class = AlarmSerializer

    def get_queryset(self):
        queryset = Alarm.objects.all()
        producer = self.request.query_params.get('producer')
        if producer is not None:
            queryset = queryset.filter(producer_id=producer)

        return queryset


class AlarmsReportView(viewsets.ViewSetMixin, generics.ListAPIView):
    serializer_class = ReportLineSerializer

    def get_queryset(self):
        alarms = Alarm.objects.all()
        alarm_list = alarms.values(
            'alarm_code').distinct().values_list('alarm_code', flat=True)
        producer_list = alarms.values(
            'producer_id').distinct().values_list('producer_id', flat=True)
        report = []

        for p in producer_list:
            producer_report = []
            for a in alarm_list:
                count = alarms.filter(alarm_code=a, producer_id=p).count()
                if count > 0:
                    aCode = AlarmCode.objects.get(id=a)
                    r = ReportLine(producer_id=p, alarm_code=aCode, count=count)
                    producer_report.append(r)
            producer_report.sort(key=lambda x: x.count, reverse=True)
            line_to_front = producer_report[:2]
            for l in line_to_front:
                a = Line()
                a.producer_id = l.producer_id
                a.alarm_code = l.alarm_code.id
                a.count = l.count
                a.level = l.alarm_code.level
                a.name = l.alarm_code.name
                a.code = l.alarm_code.code
                a.criticality = l.alarm_code.criticality
                report.append(a)
        
        logger.debug(report)
            
        return report
