DROP TABLE IF EXISTS public.api_alarmcode;
DROP TABLE IF EXISTS public.api_alarms;

CREATE table IF NOT EXISTS public.api_alarmcode (
	"level" VARCHAR (50),
	name VARCHAR (500),
	code VARCHAR (6),
	criticality VARCHAR (50),
	enabled BOOLEAN,
	producer_type_id INT,
	created_at TIMESTAMPTZ,
	created_by_id INT,
	updated_at TIMESTAMPTZ,
	updated_by_id INT,
    PRIMARY KEY (code)
    );

CREATE table IF NOT EXISTS public.api_alarms (
	"time" TIMESTAMPTZ,
    alarm_code_id VARCHAR (6),
	producer_id INT,
	created_by_id INT,
	created_at TIMESTAMPTZ,
	updated_at TIMESTAMPTZ,
	updated_by_id INT,
	CONSTRAINT fk_alarmcode
        FOREIGN KEY (alarm_code_id)
        REFERENCES public.api_alarmcode(code)
    );
    