DROP TABLE IF EXISTS public.api_alarms;
DROP TABLE IF EXISTS public.api_alarmcode;


CREATE table IF NOT EXISTS public.api_alarms (
	"time" TIMESTAMPTZ,
    alarm_code_id VARCHAR (6),
	producer_id INT,
	created_by_id INT,
	created_at TIMESTAMPTZ,
	updated_at TIMESTAMPTZ,
	updated_by_id INT
    );
    