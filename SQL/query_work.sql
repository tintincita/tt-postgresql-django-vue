SELECT alarm_code_id, producer_id,
GROUPING (producer_id, alarm_code_id),
COUNT (alarm_code_id)
FROM alarmsdb_alarm
GROUP BY ROLLUP (producer_id, alarm_code_id)
ORDER BY (producer_id, count(alarm_code_id) DESC)


SELECT alarm_code_id, producer_id,
GROUPING (producer_id, alarm_code_id),
COUNT (alarm_code_id)
FROM alarmsdb_alarm
WHERE producer_id = 41 AND alarm_code_id IS NOT NULL
GROUP BY ROLLUP (producer_id, alarm_code_id)
ORDER BY count(alarm_code_id) DESC
LIMIT 4

SELECT DISTINCT producer_id
FROM alarmsdb_alarm